from django.contrib import admin
from .models import Blog, SliderImages

admin.site.register(Blog)
admin.site.register(SliderImages)
