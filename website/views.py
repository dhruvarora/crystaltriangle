from django.shortcuts import render
from .models import SliderImages, Blog


def index(request):
    slides = SliderImages.objects.all().order_by("-date_added")[:3]
    blogs = Blog.objects.all().order_by("-date_added")[:2]

    return render(request, "index.html", {
    "slides":slides, "blogs":blogs,
    })


def contact(request):

    return render(request, "contact.html", {
    
    })
