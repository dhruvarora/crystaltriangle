from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

now = timezone.now()

class Blog(models.Model):
    date_added = models.DateField(default=now)
    title = models.CharField(max_length=150)
    intro = models.CharField(max_length=200)
    fullText = models.TextField()
    image = models.FileField(max_length=150, upload_to="blogs/images", blank=True, null=True)

    def __str__(self):
        return self.title

class SliderImages(models.Model):
    date_added = models.DateField(default=now)
    text = models.TextField()
    image = models.FileField(max_length=150, upload_to="slideshow-images")

    def __str__(self):
        return self.text
